# MaDoVi

## これは何? (What's this?)

![Icon on TaskTray](./readme_images/madovi.png)

Markdown 形式のファイルを書式化して表示するアプリケーションです。

This is simply application to display markdown format file.

## 必要な動作環境 (System Requirements)

* Windows 10
* .net 6
* Microsoft Edge WebView2 ランタイム

Microsoft Edge WebView2 ランタイム は以下の URL から evergreen バージョンをダウンロードしてください。

Download the evergreen version of the Microsoft Edge WebView2 runtime from the following URL:

https://developer.microsoft.com/en-us/microsoft-edge/webview2/

何らかの事情で Windows Update の使用できない環境でこのアプリケーションを使用する場合は
バージョン固定版のランタイムをダウンロードしてください。

バージョン固定版ランタイムを使用した場合のテストは行っていないので、ソースコートをダウンロードして
バージョン固定版ランタイムをインストールした開発環境で再コンパイルすることをお奨めします。

If you use this application in an environment where Windows Update is not available for some reason
Download the fixed version runtime.

I haven't tested it with the fixed version runtime, so download the source coat
We recommend that you recompile in the development environment where the fixed version runtime is installed.

## インストール (How to install)

このアプリケーションのインストーラはありません。
MaDoVi.exe とフォルダ内にあるすべてのファイル、サブフォルダを同一のフォルダにおいて MaDoVi.exe を実行します。

There is no installer for this application.
Run MaDoVi.exe in the same folder as MaDoVi.exe for all files and subfolders in the folder.

または、エクスプローラのファイルの関連付け、「送る」メニューに登録することも出来ます。
この場合、MaDoVi.exe のコマンドライン引数の 1 つ目に表示する Markdown ファイルのフルパスが渡されるように設定してください。

Alternatively, you can associate files in Explorer and register them in the "Send" menu.
In this case, set the full path of the Markdown file to be displayed as the first command line argument of MaDoVi.exe to be passed.


## アンインストール (How to uninstall)

このアプリケーションのアンインストーラはありません。
インストール手順で説明したフォルダを削除します。

This application does not use an uninstaller.
When deleting an application, delete the files described in the installation procedure.

以下のフォルダにアプリケーションの設定ファイルが保存されます。
設定を残す必要がないのならこのフォルダとサブフォルダを削除します。

If you do not need to leave the application settings, delete this folder and its subfolders.

``%USERPROFILE%\AppData\Roaming\TeamClishnA\MaDoVi``


## 使い方 (How to use)

コマンドライン引数を指定せずに起動した場合は空白のウィンドウが表示されます。
メニューの「ファイル」→「開く」を選択して Markdown ファイルを選択する、
またはこのウィンドウにエクスプローラから Markdown ファイル (拡張子 .md) をドロップすると
選択したファイルの内容が書式化して表示されます。

If started without command line arguments, a blank window will be displayed.

Select "File"-> "Open" from the menu and select a Markdown file,
Or if you drop a Markdown file (extension .md) from Explorer into this window
The contents of the selected file are formatted and displayed.

![Drag markdown file from exprorer](./readme_images/dragging.png)
![Dragover on window](./readme_images/droptarget.png)

**注意：**

ファイルをドロップする場合、マウスドラッグでこのアプリケーションウィンドウの外周を通るようにしてください。
下図のように、他のウィンドウが重なった状態でウィンドウ内部に直接ドロップすると正常に動作しません。

これはファイルの表示に使用している WebView2 の制限によるもので、対策を現在調査中です。

**Notes:**

When dropping a file, drag the mouse around the perimeter of this application window.
As shown in the figure below, if you drop directly inside the window with other windows overlapping, it will not work properly.

This is due to a limitation of WebView2 used to display the file, and we are currently investigating a countermeasure.


![Throwgh border of window](./readme_images/dropfrom.png)

## 設定画面 (Settings)
メニューの「ツール」→「設定」を選択するとアプリケーションの設定画面を表示します。

Select "Tools"-> "Settings" from the menu to display the application settings screen.

![Settings Dialog](./readme_images/settings.png)

### Auto Reloading

表示している Markdown を別のテキストエディタで上書き保存した場合、
新しい内容をリロードして再表示する場合はチェックをオンにします。

If you overwrite the displayed Markdown with another text editor and save it
Check this if you want to reload and redisplay the new content.

**注意:**

この機能が動作するのは開いているファイルを上書き保存した場合のみです。
別のファイルを上書きコピーした場合や、元のファイルを削除して別のファイルを同名に変更した場合は動作しません。

また、この機能はローカル PC 上の NTFS ファイルシステムを前提としています。
ファイルサーバ上の共有フォルダや FAT でフォーマットされた USB メモリ、OneDrive のオンラインのみのファイルの場合は
正常に動作せず、アプリケーションがクラッシュする可能性があるため、そのようなファイルを開く場合はチェックをオフにしてください。

**Notes:**

This feature works only when you overwrite an open file.
It will not work if you copy over another file, or if you delete the original file and change another file to the same name.

This feature also assumes an NTFS file system on your local PC.
For shared folders on the file server, FAT-formatted USB sticks, and OneDrive online-only files
Uncheck it if you want to open such files as they may not work properly and the application may crash.

### Display FileName

ファイルを開いたときにタイトルバーに表示するファイル名の表示方法を選択します。

Select how to display the file name to be displayed in the title bar when the file is opened.

例 (example):

C:\documents\repository\myApp\readme.md

というファイルを開いた場合、表示されるファイル名は設定によって以下の内容になります。

The displayed file name will be as follows depending on the settings:

  * File name only: readme.md
  * File name and parent directory name: myApp/readme.md
  * Fullpath: C:\documents\repository\myApp\readme.md

### Stylesheet

html に変換した Markdown ファイルを表示するために使用するスタイルシートの内容を設定します。
"Restore Default" ボタンを押すと初期設定が復元されます。

初期設定を復元する場合、元の設定内容はすべて失われることに注意してください。

Sets the content of the stylesheet used to display the converted Markdown file to html.
Press the "Restore Default" button to restore the default settings.

Note that if you restore the default settings, you will lose all of your original settings.

## ライセンス (License)

### MaDoVi について (About MaDoVi)

このアプリケーションはシェアウェアです。
アプリケーションを使用するための料金は株式会社ベクター様の「シェアレジ」で支払う事が出来ます。

料金は 500円 です。実際に支払う料金はシェアレジ手数料 (100円) と消費税が加算され、660円です。
料金を支払わないことによる機能の制限はなく、また料金を支払うことによる機能の追加はありません。
また、料金を支払った場合に特に必要な手続きはありません。継続して使用することができます。

Team ClishnA のシェアウェアに関する FAQ は以下の URL を参照してください。

This application is shareware.
The fee for using the application can be paid at "Share Registration" of Vector Co., Ltd.

The fee is 500 JPY. The actual fee to be paid is 660 JPY,
including the share registration fee (100 JPY) and consumption tax.

There are no functional restrictions due to not paying, and no additional features due to paying.
In addition, there is no special procedure required when paying the fee. You can continue to use it.

For FAQs on Team ClishnA shareware, please refer to the following URL.

* [FAQ](https://clishna.iplus.to/download/shareware/)

このアプリケーションはソースコードを公開しています。
このソースコードをダウンロードしてあなたはバグを修正したり自分に必要な機能を追加することができます。

このアプリケーションはオープンソースソフトウェア**ではありません**。

あなたはこのアプリケーションを配布、転載することはできません。
あなたが修正したソースコードを配布する場合、修正箇所のみを配布することができます。

This application has released the source code.
You can download the source code and fix bugs or add functions.

This application is **not** open source software.

You cannot distribute or reprint this application.
When you distribute the source code that you modified,
you can distribute only the modified part.

### アプリケーションが使用するライブラリについて (About libraries used by application)

このアプリケーションは以下のライブラリを使用しています。

This application uses the following libraries.

MarkDown 形式から html に変換するために marked を使用しています。
marked は MIT ライセンスの元で公開されています。

This application is using marked to convert from MarkDown format to html.
marked is published under the MIT license.

https://github.com/markedjs

アプリケーションデフォルトのスタイルシートの内容は以下の URL で Unilicense の元で公開されている css ファイルを使用しています。

The content of the initial stylesheet used by this application uses the css file published under Unlicense at the following URL.

https://github.com/simonlc/Markdown-CSS

アプリケーション内で html を表示するための Microsoft Edge WebView2 は以下の URL で公開されています。

The Microsoft Edge WebView2 that this application is using to display the html is published at the following URL.

https://developer.microsoft.com/en-us/microsoft-edge/webview2/

## 作者 (Author)

Team ClishnA

* [InfoClishnA](https://clishna.info/apps/madovi/)
* [Source code](https://bitbucket.org/teamclishna/madovi/)
