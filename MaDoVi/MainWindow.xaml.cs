﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MaDoVi.Windows;
using Microsoft.Web.WebView2.Core;

namespace MaDoVi
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// ウィンドウ表示にに初期表示するMarkdownファイル
        /// </summary>
        public string StartupFilePath { get; set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// WebView のコンテンツロード完了時のイベント.
        /// 読み込んだMarkdownファイルの変換処理を呼び出す
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Edge_NavigationCompleted(
            object sender,
            CoreWebView2NavigationCompletedEventArgs e)
        {
            if (this.StartupFilePath != null)
            {
                // スタートアップファイルが指定されている場合、初期化 (acout:blank のレンダリング) が完了した時点で
                // 指定したファイルを開く
                OpenFile(this.StartupFilePath);
                this.StartupFilePath = null;
            }
            else
            { ConvertMarkdown(); }
        }

        private void AddressBar_PreviewDragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effects = DragDropEffects.Copy;
                e.Handled = e.Data.GetDataPresent(DataFormats.FileDrop);
            }
            else
            { e.Effects = DragDropEffects.None; }
        }

        private void Window_PreviewDragEnter(object sender, DragEventArgs e)
        {
            // ファイルのドロップができるようにWebViewを非表示にして全面でドロップ可能にする
            Edge.Visibility = Visibility.Hidden;
        }

        private void Window_PreviewDragLeave(object sender, DragEventArgs e)
        {
            // WebView を表示状態に戻す
            Edge.Visibility = Visibility.Visible;
        }

        private void Window_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                // 単一のファイル、かつテキストファイル、MarkDownファイルの場合のみドロップ可能にする
                if (IsMarkdownFile(e.Data))
                { e.Effects = DragDropEffects.Copy; }
                else
                { e.Effects = DragDropEffects.None; }
            }
            else
            { e.Effects = DragDropEffects.None; }
        }

        private void Window_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var droppedData = e.Data.GetData(DataFormats.FileDrop) as string[];
                if (droppedData != null && 0 < droppedData.Length)
                {
                    // ドロップしたファイルを開く
                    OpenFile(droppedData[0]);
                }
            }

            // WebView を表示状態にする
            Edge.Visibility = Visibility.Visible;
        }

        private void MenuHelp_About_Click(object sender, RoutedEventArgs e)
        {
            (new AboutDialog()).ShowDialog();
        }

        private void MenuFile_Open_Click(object sender, RoutedEventArgs e)
        {
            OpenFile();
        }

        private void MenuTool_Settings_Click(object sender, RoutedEventArgs e)
        {
            (new SettingsDialog()).ShowDialog();
        }

        private void MenuFile_Exit_Click(object sender, RoutedEventArgs e)
        {
            ExitApplication();
        }
    }
}
