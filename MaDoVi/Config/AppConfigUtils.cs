﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;

namespace MaDoVi.Config
{
    /// <summary>
    /// アプリケーションの設定情報に関するユーティリティ関数群
    /// </summary>
    public static class AppConfigUtils
    {
        /// <summary>
        /// ユーザ固有のアプリケーションディレクトリを起点とした
        /// このアプリケーションの設定情報を保存するディレクトリの相対パス
        /// </summary>
        private static readonly string CONF_PATH = "./TeamClishnA/MaDoVi/Config";

        /// <summary>
        /// 設定ファイル名
        /// </summary>
        private static readonly string CONF_FILE_NAME = "madovi.conf";

        /// <summary>
        /// アプリケーションが設定ファイルを保存するためのディレクトリを返す.
        /// </summary>
        /// <param name="createIfNotExists">このディレクトリが存在しない場合、このメソッド呼び出し時点でディレクトリを作成する場合は true</param>
        /// <returns>ディレクトリの場所を示す DirectoryInfo</returns>
        public static DirectoryInfo GetConfigDirectory(bool createIfNotExists)
        {
            var configDir = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                CONF_PATH);
            var confDirInfo = new DirectoryInfo(configDir);

            if (!confDirInfo.Exists && createIfNotExists)
            { confDirInfo.Create(); }

            return confDirInfo;
        }

        /// <summary>
        /// アプリケーションの設定情報をロードする
        /// </summary>
        /// <returns>
        /// 設定ファイルから読み込んだ設定情報.
        /// ファイルが存在しない場合はデフォルトの設定情報を生成して返す.
        /// </returns>
        public static AppConfig Load()
        {
            try
            {
                var confFilePath = Path.Combine(GetConfigDirectory(true).FullName, CONF_FILE_NAME);
                return LoadFromJson<AppConfig>(confFilePath);
            }
            catch (Exception)
            {
                // 設定ファイルが存在しない場合はデフォルト設定で読み込む
                return CreateDefaultConfig();
            }
        }

        /// <summary>
        /// 指定した設定ファイルをJSON形式でアプリケーション規定の場所に書き込む
        /// </summary>
        /// <param name="conf"></param>
        public static void SaveToJson(AppConfig conf)
        {
            var configPath = Path.Combine(GetConfigDirectory(true).FullName, CONF_FILE_NAME);
            SaveToJson(conf, configPath);
        }

        /// <summary>
        /// 指定したオブジェクトをJSON形式で指定したパスに書き込む
        /// </summary>
        /// <param name="conf">書き込み対象のオブジェクト</param>
        /// <param name="path">書き込み先のフルパス</param>
        public static void SaveToJson(
            AppConfig conf,
            string path)
        {
            using (var writer = new BinaryWriter(new FileStream(path, FileMode.Create)))
            {
                var jsonBytes = JsonSerializer.SerializeToUtf8Bytes(conf, new JsonSerializerOptions() { WriteIndented = true, });
                writer.Write(jsonBytes);
            }
        }

        /// <summary>
        /// 指定したjsonファイルを読み込み、オブジェクトに変換して返す
        /// </summary>
        /// <typeparam name="T">返すオブジェクトの型</typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public static T LoadFromJson<T>(string path)
        {
            try
            {
                using (var reader = new BinaryReader(new FileStream(path, FileMode.Open)))
                {
                    var fi = new FileInfo(path);
                    var bytes = reader.ReadBytes((int)fi.Length);
                    var jsonReader = new Utf8JsonReader(bytes);
                    object result = JsonSerializer.Deserialize<T>(ref jsonReader);

                    return (T)result;
                }
            }
            catch (Exception eUnknown)
            {
                throw eUnknown;
            }
        }

        /// <summary>
        /// デフォルトの設定情報を生成して返す
        /// </summary>
        /// <returns>デフォルトの設定情報</returns>
        public static AppConfig CreateDefaultConfig()
        {
            using (var st = Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.CSS_PATH))
            using (var reader = new StreamReader(st))
            {
                // デフォルトのCSS文字列を読み込む
                var css = reader.ReadToEnd();

                // デフォルトの設定情報を生成して返す
                return new AppConfig()
                {
                    FilePathFormat = OpenedFilePathFormat.ParentDir,
                    UseFileSystemWatcher = false,
                    WaitTimeWhenReload = 1000,
                    StyleSheet = css,
                };
            }
        }
    }
}
