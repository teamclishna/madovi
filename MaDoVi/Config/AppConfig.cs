﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MaDoVi.Config
{
    /// <summary>
    /// タイトルバーに表示するファイル名の表示方式
    /// </summary>
    public enum OpenedFilePathFormat
    {
        /// <summary>ファイル名のみ</summary>
        FileNameOnly,
        /// <summary>ファイル名と一つ上のディレクトリ名</summary>
        ParentDir,
        /// <summary>フルパス</summary>
        FullPath,
    }

    /// <summary>
    /// アプリケーションの設定情報
    /// </summary>
    [Serializable]
    public class AppConfig
    {
        /// <summary>
        /// タイトルバーに表示するファイル名の表示方式
        /// </summary>
        public OpenedFilePathFormat FilePathFormat { get; set; }

        /// <summary>
        /// FileSystemWatcher のイベント発生から自動リロードを行うまでの待ち時間をミリ秒単位で設定する
        /// </summary>
        public int WaitTimeWhenReload { get; set; }
        /// <summary>
        /// 開いたファイルを FileSystemWatcher で監視するかどうか
        /// </summary>
        public bool UseFileSystemWatcher { get; set; }

        /// <summary>
        /// html 表示時に使用するスタイルシート
        /// </summary>
        public String StyleSheet { get; set; }
    }
}
