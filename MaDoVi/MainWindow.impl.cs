﻿using MaDoVi.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows;

namespace MaDoVi
{
    /// <summary>
    /// MainWindow の処理部分
    /// </summary>
    public partial class MainWindow
    {
        /// <summary>
        /// ファイルの更新を検出してリロード待ちの間trueを設定する.
        /// trueの間に再度ファイル更新を検出してもリロード処理を重複して行わないようにする.
        /// </summary>
        private bool Reloading = false;

        private FileSystemWatcher watcher = null;

        /// <summary>
        /// 指定されたファイルがMarkdowファイルであるかどうかを判定する
        /// </summary>
        /// <param name="o"></param>
        /// <returns>指定されたファイルが単一のオブジェクトである、かつ拡張子 .txt、または .md である場合に true を返す</returns>
        private bool IsMarkdownFile(object o)
        {
            var files = o as string[];
            if(files == null)
            { return false; }

            if (files.Length != 1)
            { return false; }

            var fileName = files[0].ToLower();
            return (fileName.EndsWith(".txt") || fileName.EndsWith(".md"));
        }

        /// <summary>
        /// ファイルを開くダイアログを表示する
        /// </summary>
        private void OpenFile()
        {
            var dlg = new Microsoft.Win32.OpenFileDialog()
            {
                Multiselect = false,
                FilterIndex = 1,
                Filter = "Markdown Files (*.md)|*.md",
            };
            var dlgResult = dlg.ShowDialog();
            if (dlgResult.HasValue && dlgResult.Value)
            { OpenFile(dlg.FileName); }
        }

        /// <summary>
        /// 指定したファイルを開く
        /// </summary>
        /// <param name="path"></param>
        private void OpenFile(string path)
        {
            // WebView の初期化が終わっていない場合は何もしない
            if (Edge.CoreWebView2 == null)
            { return; }

            var f = new FileInfo(path);

            // Markdown型式のファイルではない場合はタイトルバーにエラーメッセージを表示して終了する
            if (!IsMarkdownFile(new string[] { path }))
            {
                this.Title = string.Format(MaDoVi.Resources.Strings.MAIN_WINDOW_NOT_MARKDOWN, f.Name);
                return;
            }

            var conf = AppConfigUtils.Load();

            // タイトルバーを設定
            // 開いたファイル名とその一つ上のディレクトリ名を表示
            this.Title = $"{CreateFileNameView(f, conf)} - MaDoVi";

            // ファイルシステムの監視を開始
            // この時、既に別のファイルを監視中であれば解放する
            if (this.watcher != null)
            { EndWatcher(this.watcher); }

            if (conf.UseFileSystemWatcher)
            { this.watcher = StartWatcher(f); }

            Edge.CoreWebView2.Navigate(path);
        }

        /// <summary>
        /// 指定したファイルの監視を開始する
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        private FileSystemWatcher StartWatcher(FileInfo target)
        {
            var watcher = new FileSystemWatcher(target.Directory.FullName)
            {
                Filter = target.Name,
                NotifyFilter = NotifyFilters.LastWrite,
            };
            watcher.Changed += Watcher_Changed;
            watcher.EnableRaisingEvents = true;

            return watcher;
        }

        /// <summary>
        /// 監視中のファイルシステムを解放する
        /// </summary>
        /// <param name="watcher"></param>
        private void EndWatcher(FileSystemWatcher watcher)
        {
            watcher.EnableRaisingEvents = false;
            watcher.Changed -= Watcher_Changed;
        }

        /// <summary>
        /// ファイルシステムの変更を検出する
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Watcher_Changed(object sender, FileSystemEventArgs e)
        {
            // ファイル更新時に複数回のイベントが発生しても前の処理中に重複して実行しないようにする
            if(this.Reloading)
            { return; }

            this.Reloading = true;

            // イベント発生から少し間を空けてからブラウザのリロードをかける
            var conf = AppConfigUtils.Load();
            Thread.Sleep(conf.WaitTimeWhenReload);
            Edge.Dispatcher.BeginInvoke(new Action(() => { Edge.Reload(); }));
        }

        /// <summary>
        /// 指定したテキストリソースを埋め込みリソースからロードする.
        /// </summary>
        /// <returns></returns>
        private String LoadTextResource(string path)
        {
            var asm = System.Reflection.Assembly.GetExecutingAssembly();
            using (var st = asm.GetManifestResourceStream(path))
            using (var reader = new StreamReader(st))
            { return reader.ReadToEnd(); }
        }

        /// <summary>
        /// WebView で読み込んだ内容を marked.js で変換して再表示する.
        /// </summary>
        private async void ConvertMarkdown()
        {
            var conf = AppConfigUtils.Load();

            var markedJS = LoadTextResource(Constants.MARKED_JS_PATH);
            var convertJS = LoadTextResource(Constants.CONVERT_JS_PATH);
            convertJS = convertJS.Replace("%%CSS%%", conf.StyleSheet);

            await Edge.ExecuteScriptAsync(markedJS + convertJS);

            // 変換完了したらフラグを戻してファイルの更新を受け付けるようにする
            this.Reloading = false;
        }

        /// <summary>
        /// タイトルバーに表示するファイル名表示を生成する
        /// </summary>
        /// <param name="fi"></param>
        /// <param name="conf"></param>
        /// <returns></returns>
        private string CreateFileNameView(
            FileInfo fi,
            AppConfig conf)
        {
            switch(conf.FilePathFormat)
            {
                case OpenedFilePathFormat.FileNameOnly:
                    return fi.Name;
                case OpenedFilePathFormat.FullPath:
                    return fi.FullName;
                case OpenedFilePathFormat.ParentDir:
                    return $"{fi.DirectoryName.Substring(fi.DirectoryName.LastIndexOf('\\') + 1)}/{fi.Name}";
                default:
                    return fi.Name;
            }
        }

        /// <summary>
        /// アプリケーションを終了する
        /// </summary>
        private void ExitApplication()
        {
            Application.Current.Shutdown();
        }
    }
}
