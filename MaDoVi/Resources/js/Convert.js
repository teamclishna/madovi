﻿// Disable Drag & Drop
window.addEventListener('dragover', function (e) { e.preventDefault(); }, false);
window.addEventListener('drop',     function (e) { e.preventDefault(); }, false);

// append style sheet
var head = document.getElementsByTagName('head')[0];
var cssNode = document.createElement('style');
cssNode.innerHTML = `%%CSS%%`;
head.appendChild(cssNode);

// Convert markdown text and replace in body tag.
var body = document.getElementsByTagName('body')[0];
var mdContent = document.getElementsByTagName('pre')[0];
var content = mdContent.innerText;

var after = marked(content);
body.innerHTML = after;
