﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace MaDoVi
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            var mainWnd = new MainWindow();
            if (0 < e.Args.Length)
            {
                // コマンドライン引数でファイルパスが指定されている場合、このファイルを開く
                var fi = new FileInfo(e.Args[0]);
                if (fi.Exists && (fi.Attributes & FileAttributes.Directory) == 0)
                { mainWnd.StartupFilePath = fi.FullName; }
            }
            mainWnd.Show();
        }
    }
}
