﻿using System.IO;
using System.Windows;
using System.Windows.Navigation;

namespace MaDoVi.Windows
{
    /// <summary>
    /// AboutDialog.xaml の相互作用ロジック
    /// </summary>
    public partial class AboutDialog : Window
    {
        public AboutDialog()
        {
            InitializeComponent();

            // ライセンス表示
            var asm = System.Reflection.Assembly.GetExecutingAssembly();
            using (var st = asm.GetManifestResourceStream("MaDoVi.Resources.licenses.txt"))
            using (var reader = new StreamReader(st))
            {
                var text = reader.ReadToEnd();
                LicenseTextBlock.Text = text;
            }

            // アプリケーション名とバージョン情報を動的に取得して表示
            var fi = System.Diagnostics.FileVersionInfo.GetVersionInfo(typeof(App).Assembly.Location);
            this.txtAuthor.Text = fi.CompanyName;
            this.txtVersion.Text = fi.ProductVersion;
        }

        private void ManualPage_RequestNavigate(object sender, RequestNavigateEventArgs e)
        { System.Diagnostics.Process.Start(e.Uri.ToString()); }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            // このダイアログを閉じる
            this.DialogResult = true;
            this.Close();
        }
    }
}
