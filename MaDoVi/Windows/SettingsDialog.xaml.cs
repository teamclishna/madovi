﻿using MaDoVi.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MaDoVi.Windows
{
    /// <summary>
    /// SettingsDialog.xaml の相互作用ロジック
    /// </summary>
    public partial class SettingsDialog : Window
    {
        public SettingsDialog()
        {
            InitializeComponent();
            LoadSettings();
        }

        /// <summary>
        /// 設定情報を読み込んでフォーム上に表示する
        /// </summary>
        private void LoadSettings()
        {
            var config = AppConfigUtils.Load();

            this.CheckUseFileSystemWatcher.IsChecked = config.UseFileSystemWatcher;
            this.CssText.Text = config.StyleSheet;

            switch (config.FilePathFormat)
            {
                case OpenedFilePathFormat.FileNameOnly:
                    this.OptionFileNameOnly.IsChecked = true;
                    break;
                case OpenedFilePathFormat.ParentDir:
                    this.OptionFileAndDir.IsChecked = true;
                    break;
                case OpenedFilePathFormat.FullPath:
                    this.OptionFileFullpath.IsChecked = true;
                    break;
            }
        }

        /// <summary>
        /// フォームに入力されている内容をファイルに保存する
        /// </summary>
        private void SaveSettings()
        {
            var filePathFormat = (
                (this.OptionFileNameOnly.IsChecked.HasValue && this.OptionFileNameOnly.IsChecked.Value) ? OpenedFilePathFormat.FileNameOnly :
                (this.OptionFileAndDir.IsChecked.HasValue && this.OptionFileAndDir.IsChecked.Value) ? OpenedFilePathFormat.ParentDir :
                 OpenedFilePathFormat.FullPath
                );

            var newConfig = new AppConfig()
            {
                UseFileSystemWatcher = (this.CheckUseFileSystemWatcher.IsChecked.HasValue ? this.CheckUseFileSystemWatcher.IsChecked.Value : false),
                FilePathFormat = filePathFormat,
                StyleSheet = this.CssText.Text,
            };

            AppConfigUtils.SaveToJson(newConfig);
        }

        private void RestoreDefaultButton_Click(object sender, RoutedEventArgs e)
        {
            // デフォルトのスタイルシートの内容をリソースから読み込んでテキストボックスに設定する
            using (var st = Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.CSS_PATH))
            using (var reader = new StreamReader(st))
            {
                var css = reader.ReadToEnd();
                this.CssText.Text = css;
            }
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            // 入力内容を設定ファイルに保存してダイアログを閉じる
            SaveSettings();
            this.DialogResult = true;
            this.Close();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            // ダイアログの内容を保存せずにこのまま閉じる
            this.DialogResult = false;
            this.Close();
        }
    }
}
