﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MaDoVi
{
    /// <summary>
    /// 定数定義群
    /// </summary>
    public static class Constants
    {
        /// <summary>marked.js のリソース名</summary>
        public static readonly string MARKED_JS_PATH = "MaDoVi.Resources.js.marked.js";

        /// <summary>読み込んだ MarkDown ファイルを変換するための JavaScript のリソース名</summary>
        public static readonly string CONVERT_JS_PATH = "MaDoVi.Resources.js.Convert.js";

        /// <summary>デフォルトのスタイルシートのリソース名</summary>
        public static readonly string CSS_PATH = "MaDoVi.Resources.css.markdown_default.css";
    }
}
